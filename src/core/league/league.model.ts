import * as mongoose from 'mongoose';
import { Team } from '../team/team.model';

export const LeagueSchema = new mongoose.Schema({
    name:String,
    sport:String,
    teams:Array
});

export interface League extends mongoose.Document{
    name:string,
    sport:string,
    teams: Team[]
}