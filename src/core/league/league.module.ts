import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LeagueController } from './league.controller';
import { LeagueSchema } from './league.model';
import { LeagueService } from './league.service';




@Module({
    imports:[
        MongooseModule.forFeature([{name:'League' , schema:LeagueSchema }])
    ],
    controllers: [LeagueController],
    providers:[LeagueService]
})

export class LeagueModule {}