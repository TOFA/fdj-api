import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { League } from './league.model';

@Injectable()

export class LeagueService{
    constructor(
        @InjectModel('League')private readonly  leagueModel: Model<League>
        ){}
    
    async getLeagues(){
        return await this.leagueModel.find().exec();
    }

}