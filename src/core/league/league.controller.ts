import { Controller, Get } from "@nestjs/common";
import { LeagueService } from "./league.service";

@Controller('leagues')
export class LeagueController{
    constructor( private readonly leagueService : LeagueService){}
    @Get()
    async getLeagues(){
        const leagues = this.leagueService.getLeagues();
        return leagues;
    }

}
