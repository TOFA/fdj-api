import * as mongoose from 'mongoose';

export const PlayerSchema = new mongoose.Schema({
    born:String,
    name:String,
    position:String,
    signin:Object,
    thumbnail:String
});

export interface Player extends mongoose.Document{
    born:String,
    name:String,
    position:String,
    signin:Object,
    thumbnail:String
}