import { Controller, Get, Query } from "@nestjs/common";
import { PlayerService } from "./player.service";

@Controller('players')
export class PlayerController{
    constructor( private readonly playerService : PlayerService){}
    @Get()
    async getPlayersByTeam( @Query('playersIdArray') playersIdArray:string){
        const players =  this.playerService.getPlayersByTeam(JSON.parse(playersIdArray));
        return players;
    }

}
