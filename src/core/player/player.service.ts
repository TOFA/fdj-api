import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Player } from './player.model';

@Injectable()
export class PlayerService{

    constructor(
        @InjectModel('Player')private readonly  playerModel: Model<Player>
        ){}

    async getPlayersByTeam(ids : string[]){
        return await this.playerModel.find({ _id: {$in:ids} }).exec();
    }
}