import * as mongoose from 'mongoose';
import { Player } from '../player/player.model';

export const TeamSchema = new mongoose.Schema({
   
    name:String,
    players:Array,
    thumbnail:String
});

export interface Team extends mongoose.Document{
    _id:String
    name:String,
    players:Player[],
    thumbnail:String
}