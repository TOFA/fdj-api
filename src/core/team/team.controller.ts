import { Controller, Get } from "@nestjs/common";
import { Query } from "@nestjs/common/decorators/http/route-params.decorator";
import { TeamService } from "./team.service";

@Controller('teams')
export class TeamController{
    constructor( private readonly teamService : TeamService){}
    
    @Get()
    async getTeamsByLeague( @Query('teamsIdArray') teamsIdArray:string){
        const teams =  await this.teamService.getTeamsByLeague(JSON.parse(teamsIdArray));
        return teams;
    }

}
