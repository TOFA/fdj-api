
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Team } from './team.model';


@Injectable()
export class TeamService{

    constructor(
        @InjectModel('Team')private readonly  teamModel: Model<Team>
        ){}
    async getTeamsByLeague(ids : string[]){
        console.log(' ids getTeamsByLeague',ids.length)
        return await this.teamModel.find({ _id: {$in:ids} }).exec();

    }
}