import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { LeagueModule } from './core/league/league.module';
import { PlayerModule } from './core/player/player.module';
import { TeamModule } from './core/team/team.module';

@Module({
  imports: [
    PlayerModule,
    LeagueModule,
    TeamModule,
    MongooseModule.forRoot('mongodb://localhost:27017/sports')
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
